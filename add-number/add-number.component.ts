import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AngularFireDatabase} from 'angularfire2/database';
@Component({
  selector: 'app-add-number',
  templateUrl: './add-number.component.html',
  styleUrls: ['./add-number.component.css']
})
export class AddNumberComponent  {
  constructor(private db: AngularFireDatabase) { }
  title = 'NodeMCU';
  addNumber(data:NgForm){
    this.db.list("/nodemcu-aa998").set("number",data.value.number);
    console.log(data.value.number);
  }
}
