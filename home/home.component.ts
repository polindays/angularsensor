import { Component, OnInit } from "@angular/core";
import { map } from "rxjs/operators";
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { noComponentFactoryError } from "@angular/core/src/linker/component_factory_resolver";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  read: AngularFireList<any>;
  moves="";
  number="";
  no;
  fact = true;
  get buttonClass() {
    if (this.no<3) return 'fair';
    else if (this.no <5) return 'medium';
    else return 'dense'; 
  }

  constructor(private db: AngularFireDatabase) {
    this.read = db.list("/nodemcu-aa998");
  }

  addNum() {
    this.db.list("/nodemcu-aa998").set("number",(this.no + 1).toString() );
    console.log(this.no);
  }

  ngOnInit() {
    this.startTimer();
    this.initNumer();
    this.read
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(action => ({
            key: action.key,
            value: action.payload.val()
          }));
        })
      )
      .subscribe(items => {
        this.moves = items[1].value;
        this.fact = this.moves == "Ready";
        if(this.fact && this.no!=0) {this.no=0;
          this.db.list("/nodemcu-aa998").set("number", this.no.toString());
        }
        if (this.fact){
        this.timeLeft = 0;
        this.pauseTimer();
        this.startTimer();
        if (this.fact) {
          this.playAudio();
        }
      }
      });
      this.no=+this.number;
  }
  timeLeft: number = 0;
  interval;
  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft < 9999) {
        this.timeLeft++;
      }
    }, 60000);
  }
  pauseTimer() {
    clearInterval(this.interval);
  }
  playAudio() {
    let audio = new Audio();
    audio.src = "../../../assets/decay.wav";
    audio.load();
    audio.play();
  }
  initNumer(){
    this.read
    .snapshotChanges()
    .pipe(
      map(actions => {
        return actions.map(action => ({
          key: action.key,
          value: action.payload.val()
        }));
      })
    )
    .subscribe(items => {
      this.number = items[0].value;
      this.no = +this.number;
    });
  
  }
  increase() {
    this.initNumer();
      this.addNum();
  }
}
