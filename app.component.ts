import { Component } from '@angular/core';
import { NgForm }   from '@angular/forms';
import {AngularFireDatabase} from 'angularfire2/database';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private db: AngularFireDatabase){}
  title = 'NodeMCU';
  addMovement(data:NgForm){
    this.db.list("/nodemcu-aa998").push(data.value);
  }
}
