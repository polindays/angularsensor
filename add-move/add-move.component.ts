import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AngularFireDatabase} from 'angularfire2/database';
@Component({
  selector: 'app-add-move',
  templateUrl: './add-move.component.html',
  styleUrls: ['./add-move.component.css']
})
export class AddMoveComponent  {

  constructor(private db: AngularFireDatabase) { }
  title = 'NodeMCU';
  addMovement(data:NgForm){
    this.db.list("/nodemcu-aa998").set("status",data.value.status);
  }

}
